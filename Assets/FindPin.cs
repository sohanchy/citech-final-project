using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPin : Step
{
    public GameObject Balloonhint;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the Connecting Pin.";

        Targets.ActivateTarget("balloon");
        Balloonhint.SetActive(true);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("balloon");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Balloonhint.SetActive(false);
    }
}
