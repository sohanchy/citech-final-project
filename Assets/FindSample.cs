using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FindSample : Step
{
    public GameObject MoonHint;
    public GameObject Sample;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the sample,check the QR code.";

        Targets.ActivateTarget("moon");
        MoonHint.SetActive(true);
        Sample.SetActive(false);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("moon");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        MoonHint.SetActive(false);
        //Sample.SetActive(true);
    }
}

