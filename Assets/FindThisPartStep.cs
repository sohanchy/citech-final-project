using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindThisPartStep : Step
{
    public GameObject LightningHint;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the part being shown.";

        Targets.ActivateTarget("lightning");
        LightningHint.SetActive(true);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("lightning");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        LightningHint.SetActive(false);
    }
}
