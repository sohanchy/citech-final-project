using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findthelowerring : Step
{
    public GameObject Playhint;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the Ring , check for the QR Code being shown.";

        Targets.ActivateTarget("play");
        Playhint.SetActive(true);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("play");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Playhint.SetActive(false);
    }
}

