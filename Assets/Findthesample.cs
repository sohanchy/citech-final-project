using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindThisPartStep1 : Step
{
    public GameObject MoonHint;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the sample , please check for the QR Code.";

        Targets.ActivateTarget("Moon");
        MoonHint.SetActive(true);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("Moon");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        MoonHint.SetActive(false);
    }
}