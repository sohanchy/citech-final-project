using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToMachineStep : Step
{
    protected override void StartStepInternal()
    {
        Instructions.text = "Look at the machine top";

        Targets.ActivateTarget("machineTop");
    }

    public override bool StepLoop()
    {
        if (Targets.IsBeingTracked("machineTop"))
        {
            Targets.ActivateTarget("machineBottom");
            Instructions.text = "Look at the machine bottom(while top in focus)";
            if (Targets.IsBeingTracked("machineBottom") && Targets.IsBeingTracked("machineTop"))
            {
                return true;
            }
        }

        return false;
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("machineTop");
        //Targets.DeactivateTarget("machineBottom");
    }
}
