using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePin : Step
{
    public GameObject Arrow;

    public GameObject PlayTarget;

    public GameObject Grip2;
    GameObject mb;
    
    [SerializeField]
    float closenessThreshold;
    protected override void StartStepInternal()
    {
        Instructions.text = "Move this pin to the ring";

        Targets.ActivateTarget("balloon");
        Targets.ActivateTarget("play");
        
        Arrow.SetActive(true);

        mb = Targets.GetTarget("play");

        Arrow.transform.SetParent(mb.transform);
        //Arrow.transform.position = new Vector3(0,5,5);

        Arrow.transform.LookAt((mb.transform.position - new Vector3(200, 0, 0)) - mb.transform.position);


        //Arrow.transform.position = Arrow.transform.position + new Vector3(-0.12f, -0.02f,0);
        Arrow.transform.Rotate(0, 0, 90);

        Grip2.SetActive(false);
    }

    public override bool StepLoop()
    {
        GameObject balloon = Targets.GetTarget("balloon");
        float distance2 = Vector3.Distance(balloon.transform.position, mb.transform.position);

        
        if(Targets.IsBeingTracked("balloon") && Targets.IsBeingTracked("play"))
        {
            
            if (distance2 < closenessThreshold)
            {
                
                Instructions.text = "Correct!";

                PlayTarget.SetActive(false);
                Grip2.SetActive(true);
                return true;
            }
            else if (distance2 < closenessThreshold * 2)
            {
                Instructions.text = "Close, but move even more closer";
            }


        }
        return false;
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Arrow.SetActive(false);
        Targets.ActivateTarget("play");
    }
}