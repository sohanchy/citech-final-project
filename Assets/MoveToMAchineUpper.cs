using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMAchineUpper : Step

{
    public GameObject StarHint;
    protected override void StartStepInternal()
    {
        Instructions.text = "Find the upper fixture and Check the QR code shown .";

        Targets.ActivateTarget("star");
        StarHint.SetActive(true);
    }

    public override bool StepLoop()
    {
        return Targets.IsBeingTracked("star");
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        StarHint.SetActive(false);
    }
}