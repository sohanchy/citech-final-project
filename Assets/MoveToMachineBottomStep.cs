using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMachineBottomStep : Step
{
    public GameObject GrippyBoi;
    public GameObject Arrow;
    GameObject mb;
    
    [SerializeField]
    float closenessThreshold;
    protected override void StartStepInternal()
    {
        Instructions.text = "Move this object to the machine bottom";

        Targets.ActivateTarget("lightning");
        Targets.ActivateTarget("machineBottom");
        
        Arrow.SetActive(true);
        GrippyBoi.SetActive(false);

        mb = Targets.GetTarget("machineBottom");

        Arrow.transform.SetParent(mb.transform);
        //Arrow.transform.position = new Vector3(0,5,5);

        Arrow.transform.LookAt((mb.transform.position - new Vector3(200, 0, 0)) - mb.transform.position);


        //Arrow.transform.position = Arrow.transform.position + new Vector3(-0.12f, -0.02f,0);
        Arrow.transform.Rotate(0, 0, 90);
    }

    public override bool StepLoop()
    {
        GameObject balloon = Targets.GetTarget("balloon");
        float distance = Vector3.Distance(balloon.transform.position, mb.transform.position);

        
        if(Targets.IsBeingTracked("balloon") && Targets.IsBeingTracked("machineBottom"))
        {
            
            if (distance < closenessThreshold)
            {
                GrippyBoi.SetActive(true);
                Instructions.text = "Correct!";
                return true;
            }
            else if (distance < closenessThreshold * 2)
            {
                Instructions.text = "Close, but move even more closer";
            }


        }
        return false;
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Arrow.SetActive(false);
        Targets.ActivateTarget("machineBottom");
    }
}
