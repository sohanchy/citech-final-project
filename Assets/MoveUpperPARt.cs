using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpperPARt : Step
{
    public GameObject Arrow;
    GameObject mb;
    public GameObject GrippyBoi;
    
    [SerializeField]
    float closenessThreshold;
    protected override void StartStepInternal()
    {
        Instructions.text = "Move this Upper Fixture to the Machine Top Fixture";

        Targets.ActivateTarget("star");
        Targets.ActivateTarget("machineTop");
        GrippyBoi.SetActive(false);
        
        Arrow.SetActive(true);

        mb = Targets.GetTarget("machineTop");

        Arrow.transform.SetParent(mb.transform);
        //Arrow.transform.position = new Vector3(0,5,5);

        Arrow.transform.LookAt((mb.transform.position - new Vector3(200, 0, 0)) - mb.transform.position);


        //Arrow.transform.position = Arrow.transform.position + new Vector3(-0.12f, -0.02f,0);
        Arrow.transform.Rotate(0, 0, 90);
    }

    public override bool StepLoop()
    {
        GameObject star = Targets.GetTarget("star");
        float distance4 = Vector3.Distance(star.transform.position, mb.transform.position);

        
        if(Targets.IsBeingTracked("star") && Targets.IsBeingTracked("machineTop"))
        {
            
            if (distance4 < closenessThreshold)
            {
                GrippyBoi.SetActive(true);
                Instructions.text = "Correct!";
                return true;
            }
            else if (distance4 < closenessThreshold * 2)
            {
                Instructions.text = "Close, but move even more closer";
            }


        }
        return false;
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Arrow.SetActive(false);
        Targets.ActivateTarget("machineBottom");
    }
}
