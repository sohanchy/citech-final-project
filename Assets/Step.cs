using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Step: MonoBehaviour
{
    protected Targets Targets;
    protected UnityEngine.UI.Text Instructions;
    private bool isShown = false;
    public virtual void Setup(Targets targets, UnityEngine.UI.Text instructions)
    {
        Targets = targets;
        Instructions = instructions;
    }
    public void StartStep()
    {
        if (!isShown)
        {
            StartStepInternal();
            isShown = true;
            Debug.Log("IN STEP: ---- " + GetType().Name);
        }
    }

    public void FinishStep() {
        isShown = false;
        Instructions.text = "";
        FinishStepInternal();
    }

    protected abstract void StartStepInternal();

    // Return true ONLY if step is finished
    public abstract bool StepLoop();
    public abstract void FinishStepInternal();
    
}
