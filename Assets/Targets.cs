using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets : MonoBehaviour
{
    public UnityEngine.UI.Text CurrentlyTracked;
    public enum TrackingStatus
    {
        NotTracked,
        Tracked,
        TrackingLost
    };

    IDictionary<string, TrackingStatus> TargetsDict = new Dictionary<string, TrackingStatus>();

    private void Start()
    {
        foreach(Transform child in transform)
        {
            TargetsDict.Add(child.gameObject.name, TrackingStatus.NotTracked);

            Debug.Log(child.gameObject.name);
        }

        
    }
    public void DeactivateTarget(string targetName)
    {
        transform.Find(targetName).gameObject.SetActive(false);
    }

    public GameObject GetTarget(string targetName)
    {
        return transform.Find(targetName).gameObject;
    }

    public void ActivateTarget(string targetName)
    {
        transform.Find(targetName).gameObject.SetActive(true);
    }

    public bool IsBeingTracked(string targetName)
    {
        return GetStatus(targetName) == TrackingStatus.Tracked;
    }

    public TrackingStatus GetStatus(string targetName)
    {
        TrackingStatus status;
        if (TargetsDict.TryGetValue(targetName, out status))
        {
            return status;
        }
        else
        {
            throw new System.Exception("Invalid Target Name: "+ targetName + " - " 
                + (new System.Diagnostics.StackTrace()).GetFrame(2).GetMethod().Name);
        }
    }

    public void SetTrackingStatus(string targetAndStatus)
    {
        string[] query = targetAndStatus.Split(',');

        if ( !IsValidTarget(query[0]) ){
            return;
        }
        TargetsDict[query[0]] = ConvertStringToTrackingStatus(query[1]);



        // Visual stuff, move later
        //if(TargetsDict[query[0]] == TrackingStatus.Tracked)
        //{
        //    GameObject tmp = GetTarget(query[0]);
        //    //tmp.transform.
        //}


        string txt = "BeingTracked: ";
        foreach (var target in TargetsDict)
        {
            if(target.Value == TrackingStatus.Tracked)
            {
                txt = txt + target.Key + ", ";
            }
        }
        CurrentlyTracked.text = txt;
    }

    private static TrackingStatus ConvertStringToTrackingStatus(string statusString)
    {
        switch (statusString.ToLower())
        {
            case "trackinglost":
                return TrackingStatus.TrackingLost;
            case "tracked":
                return TrackingStatus.Tracked;
            default:
                return TrackingStatus.NotTracked;
        }
    }

    private bool IsValidTarget(string targetName)
    {
        foreach (Transform target in transform)
        {
           if (target.gameObject.name == targetName)
            {
                return true;
            }
        }
        return false;
    }
}
