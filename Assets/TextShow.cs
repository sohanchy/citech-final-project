using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TextShow : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DetectedStar()
    {
        Debug.Log("STAR");
        DetectedMarker("star");
    }

    public void DetectedPlay()
    {
        DetectedMarker("play");
    }

    public void DetectedLightning()
    {
        DetectedMarker("lightning");
    }

    public void DetectedMoon()
    {
        DetectedMarker("moon");
    }

    public void DetectedMachine()
    {
        DetectedMarker("machine");
    }

    public void DetectedBalloon()
    {
        DetectedMarker("balloon");
    }

    private void DetectedMarker(string name)
    {
        gameObject.GetComponent<UnityEngine.UI.Text>().text = name;
    }

    public void ClearText()
    {
        Debug.Log("CLEAR");
        gameObject.GetComponent<UnityEngine.UI.Text>().text = "gone";
    }
}
