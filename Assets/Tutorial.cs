using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public UnityEngine.UI.Text Instructions;

    public Step currentStep;
    public int currentStepIndex;

    public GameObject TargetsGameObject;
    private Targets Targets;
    private List<Step> StepList = new List<Step>();

    // Start is called before the first frame update
    void Start()
    {
        Targets = TargetsGameObject.GetComponent<Targets>();
        foreach (Transform child in transform)
        {
            Step step = child.gameObject.GetComponent<Step>();
            step.Setup(Targets,Instructions);
            StepList.Add(step);

            Debug.Log("ADDING STEP---------" + step.GetType().Name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!currentStep)
        {
            currentStep = StepList[currentStepIndex];
        }

        currentStep.StartStep();

        if (currentStep.StepLoop())
        {
            NextStep();
        }

    }

    public void NextStep()
    {
        currentStep.FinishStep();

        if (!(currentStepIndex + 1 < StepList.Count))
        {
            Instructions.text = "ALL STEPS COMPLETE";
            Debug.Log("IN FINAL STEP, NO NEXT");
            return;
        }

        
        currentStepIndex++;
        currentStep = StepList[currentStepIndex];

        Debug.Log("NEXT STEP");
        
    }

    public void PreviousStep()
    {
        currentStep.FinishStep();
        if (currentStepIndex == 0)
        {
            Debug.Log("IN FIRST STEP, NO PREV.");
            return;
        }

        currentStepIndex--;
        currentStep = StepList[currentStepIndex];

        Debug.Log("PREV STEP");
    }

}
