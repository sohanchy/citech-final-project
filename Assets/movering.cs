using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movering : Step
{
    public GameObject Arrow;
    GameObject mb1;
    
    [SerializeField]
    float closenessThreshold;
    protected override void StartStepInternal()
    {
        Instructions.text = "Move this object to the bottom Fixture";

        Targets.ActivateTarget("play");
        Targets.ActivateTarget("lighting");
        
        Arrow.SetActive(true);

        mb1 = Targets.GetTarget("lighting");

        Arrow.transform.SetParent(mb1.transform);
        //Arrow.transform.position = new Vector3(0,5,5);

        Arrow.transform.LookAt((mb1.transform.position - new Vector3(200, 0, 0)) - mb1.transform.position);


        //Arrow.transform.position = Arrow.transform.position + new Vector3(-0.12f, -0.02f,0);
        Arrow.transform.Rotate(0, 0, 90);
    }

    public override bool StepLoop()
    {
        GameObject play = Targets.GetTarget("play");
        float distance1 = Vector3.Distance(play.transform.position, mb1.transform.position);

        
        if(Targets.IsBeingTracked("play") && Targets.IsBeingTracked("lighting"))
        {
            
            if (distance1 < closenessThreshold)
            {
                
                Instructions.text = "Correct!";
                return true;
            }
            else if (distance1 < closenessThreshold * 2)
            {
                Instructions.text = "Close, but move even more closer";
            }


        }
        return false;
    }

    public override void FinishStepInternal()
    {
        //Targets.DeactivateTarget("star");
        Arrow.SetActive(false);
        Targets.ActivateTarget("lighting");
    }
}