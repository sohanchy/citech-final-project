## Setup Instructions

 1. Install Unity Hub: 
 https://unity3d.com/get-unity/download
 2. Open Unity Hub and install Unity Editor 2020.3.26f1 LTS
 3. In the install wizard check JDK and Android SDK
 4. Clone this project's github repo and open in Unity: https://gitlab.com/sohanchy/citech-final-project
 5. Open a Vuforia developer account
 https://developer.vuforia.com/
 6. Download vuforia package for unity and double click to install.
 7. In Vuforia developer portal create a personal license and add it to the project https://library.vuforia.com/articles/Solution/How-To-add-a-License-Key-to-your-Vuforia-App.html
 8. Print the qr codes onto white paper, cut them to square shapes for usage.![Print the qr codes onto white paper, cut them to square shapes for usage](https://i.imgur.com/lmaKBQl.png)
 9. Switch unity build platform to Android, connect an Android device, build and run to test that everything is working properly.

 
## Drag and drop System
![](https://i.imgur.com/xk16yjv.png)
The Tutorial Gameobject is the primary holder of a step by step tutorial. Each child component is a Step in the tutorial. There are different pre-built types of Steps available:
 - Find X Part
 - Move Objects to X Location
 - Move Two Objects Close/Far
Other step types can easily be built by extending the Step class. You can copy paste one of the pre-built step types with the scripts, rename the gameobject and script to your new step name. Drag and order the step inside tutorial object as needed.

**Move Two Objects Close/Far**
Just drag and drop the two gameobjects and set a closeness threshold of how close you want them. If you want to move far, then just change the threshold < / > signs inside the script.
**Find X Part**
Drag and drop whichever part needs to be found to the Script section. Check the script, it is very simple to understand. A hint can also be added.
**Move Objects to X Location**
Drag and drop an object and set the location where it needs to be moved.
**Create New Step Type**
Check the *Step* Abstract class, and the samples. Also check below class documentation.

## Target System
Check the **Targets** gameobject, add any vuforia object under this gameobject.
There is a Targets helper class that will allow you to easily manage targets without going into vuforia internal complexities.
It provides you:

 - Get any target by name: **GetTarget(string targetName)**
 - **IsBeingTracked:** Tracking status is provided as a wrapped custom Enum:
	 - NotTracked
	 - Tracked
	 - TrackingLost
 - Target status switching by name: 
	- ActivateTarget(string targetName)
	- DeactivateTarget(string targetName)
 
## Classes
**Tutorial.cs**
 - currentStep: Holds the current Step game object.
 - currentStepIndex:
   Index of the currently active step. NextStep: This method moves the tutorial to the next step. This will initiate the Finish(Cleanup) method of current step, StartStepSetup) of next step.
  - PreviousStep: This method moves the tutorial to the previous step. This will initiate the Finish(Cleanup) method of current step,
   StartStep(Setup) of previous step.

**Step.cs**
This is an abstract class that needs to be extended for making the steps for the tutorial. Some sample step types are provided. This takes heavy inspiration from Arduino style programming to make it more easy to grasp for other backgrounds than CS.

- StartStepInternal: This method must be implemented and it will have any necessary setup work such as activating targets, changing texts, etc
 - FinishStepInternal: This method must be implemented and it will have any necessary clean up finishing work such as deactivating targets, changing texts back to default, clearing any step specific game objects or other artifacts, leaving the system clean for the next step.
  - StepLoop: This method must be implemented and it should have the logical checks of the step, if this method returns true 	then the step will be considered as finished, as long as it returns false, it will be called on every frame.
