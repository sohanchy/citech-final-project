# Usage Instructions

Download the APK file to an Android device with a back camera and support for AR Core.
APK Link: https://ubcca-my.sharepoint.com/:u:/g/personal/sohan_chowdhury_ubc_ca/EfU6Tu4Dsd9Fts-xjDTErrYBB6wSY4j3oltaRUd_JCMoww?e=xjMfoG


## QR Codes

![Print the qr codes onto white paper, cut them to square shapes for usage](https://i.imgur.com/lmaKBQl.png)
Print the qr codes onto white paper, cut them to square shapes for usage.

## Setup Environment
Place the Machine Upper Part And Lower Part QR codes onto a vertical board or a wall, top part in top and bottom part in bottom with 5-8 inch distance in between.
Place other QR codes out of sight of camera but in an easy to access location.

## How to Use

 1. Install the APK 
 2. Open the APP
 3. Follow on screen instructions
